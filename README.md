[![pipeline](https://gitlab.com/apoirier42/ci-r/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/apoirier42/ci-r)
[![coverage](https://gitlab.com/apoirier42/ci-r/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/apoirier42/ci-r)

# CI R

Explore how to use GitLab CI on R code.

## Reports

### lintr report

The output of the execution of `lintr` is saved here:

![lintr.txt](https://gitlab.com/apoirier42/ci-r/builds/artifacts/master/raw/public/lintr.txt?job=pages)

### Coverage

[https://apoirier42.gitlab.io/ci-r/coverage.html](https://apoirier42.gitlab.io/ci-r/coverage.html)

